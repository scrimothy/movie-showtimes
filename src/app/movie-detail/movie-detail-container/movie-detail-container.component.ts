import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-movie-detail-container',
  templateUrl: './movie-detail-container.component.html',
  styleUrls: ['./movie-detail-container.component.scss']
})
export class MovieDetailContainerComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
