export interface IMovieImage {
  image_orientation: 'portrait' | 'landscape';
  medium: IMovieImageMetadata;
  region: string;
}

export interface IMovieImageMetadata {
  film_image: string;
  height: number;
  width: number;
}

export interface IMovieRating {
  age_advisory: string;
  age_rating_image: string;
  rating: string;
}

export interface IMovieGluMovie {
  age_rating: IMovieRating[];
  film_id: number;
  film_name: string;
  film_trailer: string;
  images: {
    poster: { [key: string]: IMovieImage },
    still: { [key: string]: IMovieImage },
  };
  imdb_id: number;
  imdb_title_id: string;
  other_titles: any;
  release_dates: any[];
  synopsis_long: string;
}

export interface IMovieGluCinema {
  cinema_id: string;
  cinema_name: string;
  address: string;
  address2: string;
  city: string;
  county: string;
  postcode: string;
  lat: string;
  lng: string;
  distance: string;
  logo_url: string;
}
