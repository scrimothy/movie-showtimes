import { animate, state, style, transition, trigger } from '@angular/animations';

export const collapseAnimation = [
  trigger('collapse', [
    state('expanded', style({
      height: '60%',
    })),
    state('collapsed', style({
      height: '0%',
    })),
    transition('* => *', [
      animate('120ms')
    ]),
  ]),
];
