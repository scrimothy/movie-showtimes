import { IMovieGluCinema } from './../../models/movie.model';
import { MovieService } from './../../movie.service';
import { Component, OnInit } from '@angular/core';
import { from, Observable } from 'rxjs';
import { IMovieGluMovie } from 'src/app/models/movie.model';

@Component({
  selector: 'app-movie-list-container',
  templateUrl: './movie-list-container.component.html',
  styleUrls: ['./movie-list-container.component.scss']
})
export class MovieListContainerComponent implements OnInit {
  movies$: Observable<IMovieGluMovie[]>;
  cinemas$: Observable<IMovieGluCinema[]>;

  constructor(private movieService: MovieService) { }

  ngOnInit(): void {
    this.movies$ = this.movieService.movies$;
    this.movieService.updateMovies();

    this.cinemas$ = this.movieService.cinemas$;
    // this.movieService.updateCinemaShowTimes();
  }



}
