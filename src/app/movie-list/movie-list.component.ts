import { IMovieGluMovie } from './../models/movie.model';
import { Component, ElementRef, HostListener, Input, OnInit } from '@angular/core';
import { fromEvent } from 'rxjs';

@Component({
  selector: 'app-movie-list',
  templateUrl: './movie-list.component.html',
  styleUrls: ['./movie-list.component.scss']
})
export class MovieListComponent implements OnInit {
  @Input() movies: IMovieGluMovie[];

  private _details = false;
  public get details() {
    return this._details;
  }
  public set details(value) {
    this._details = value;
    console.log(value);
  }

  scrollLeft = 0;
  activeIndex = 0;

  constructor(private el: ElementRef) { }

  ngOnInit(): void {
    fromEvent(this.el.nativeElement, 'scroll').subscribe((value: any) => {
      this.scrollLeft = value.target.scrollLeft;
      this.activeIndex = Math.round(this.movies.length * this.scrollLeft / (this.el.nativeElement.scrollWidth));
      console.log(this.activeIndex);
    });
  }

}
