import { MovieService } from './../movie.service';
import { collapseAnimation } from './../models/animations';
import { Component, EventEmitter, HostBinding, HostListener, Input, OnInit, Output } from '@angular/core';
import { IMovieGluMovie } from '../models/movie.model';

@Component({
  selector: 'app-movie-summary',
  templateUrl: './movie-summary.component.html',
  styleUrls: ['./movie-summary.component.scss'],
  animations: collapseAnimation,
})
export class MovieSummaryComponent implements OnInit {
  private _details = false;

  @Input() movie: IMovieGluMovie;

  @HostBinding('class.show-details')
  @Input()
  public get details() {
    return this._details;
  }
  public set details(value) {
    this._details = value;
    this.detailsChange.emit(value);
  }

  @Output() detailsChange: EventEmitter<boolean> = new EventEmitter();

  showImage = true;
  times: any[] = [
    { start: '11:20am', end: '12:58' },
    { start: '1:20pm', end: '2:58' },
    { start: '3:20pm', end: '4:58' },
  ];

  constructor(private movieService: MovieService) { }

  ngOnInit(): void {
  }

  @HostListener('swipeup')
  swipeUp() {
    this.details = true;
  }
  @HostListener('swipedown')
  swipeDown() {
    this.details = false;
  }

}
