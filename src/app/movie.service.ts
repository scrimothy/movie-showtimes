import { Injectable } from '@angular/core';
import { from, Subject } from 'rxjs';
import { IMovieGluCinema, IMovieGluMovie } from './models/movie.model';

@Injectable({
  providedIn: 'root'
})
export class MovieService {
  private defaultLocation = '-22.0;14.0';
  private baseUrl = 'https://api-gate2.movieglu.com/';
  private headers: Headers = new Headers({
    'api-version': 'v200',
    client: 'QIOB',
    mode: 'no-cors',
  });

  private evalAuth = {
    'x-api-key': '4CnMI2YiLw88lNn2UIQ6E1SNaD57K28TvovUrUh6',
    authorization: 'Basic UUlPQjpzb2s5ckpjUkZuazI=',
    territory: 'US',
  };
  private sandboxAuth = {
    'x-api-key': 'KEvlku0pUladIUgoXIUaB1m3VHpQ2Lmms87cH2d1',
    authorization: 'Basic UUlPQl9YWDpkaXY4RW1zRGp2OHU=',
    territory: 'XX',
  };


  private _movies$: Subject<IMovieGluMovie[]> = new Subject();
  private _cinemas$: Subject<IMovieGluCinema[]> = new Subject();

  movies$ = from(this._movies$);
  cinemas$ = from(this._cinemas$);

  constructor() { }

  private setHeaders(
    location: string = this.defaultLocation,
    date: string = (new Date()).toDateString(),
    env: 'sandbox' | 'eval' = 'sandbox',
  ) {
    this.headers.append('device-datetime', date);
    this.headers.append('geolocation', location);
    this.headers.append('x-api-key', this[`${env}Auth`]['x-api-key']);
    this.headers.append('authorization', this[`${env}Auth`].authorization);
    this.headers.append('territory', this[`${env}Auth`].territory);
  }

  private async fetchMovies(
    location: string = this.defaultLocation,
    date: string = (new Date()).toDateString(),
    count: number = 10,
  ): Promise<IMovieGluMovie[]> {

    this.setHeaders(location, date);


    const response = await fetch(`${this.baseUrl}filmsNowShowing/?n=${count}`, {
      headers: this.headers,
    });

    return (await response.json()).films;
  }

  private async fetchCinemasNearby(
    location: string = this.defaultLocation,
    count: number = 10,
  ): Promise<IMovieGluCinema[]> {

    this.setHeaders(location);
    const response = await fetch(`${this.baseUrl}cinemasNearby/?n=${count}`, {
      headers: this.headers,
    });

    return (await response.json()).cinemas;
  }

  private async fetchCinemaShowTimes(
    cinemaId: string = '8923',
    count: number = 10,
  ): Promise<IMovieGluCinema[]> {

    // this.setHeaders(location);
    const response = await fetch(`${this.baseUrl}cinemaShowTimes/?cinema_id=${cinemaId}&n=${count}`, {
      headers: this.headers,
    });

    return (await response.json()).cinemas;
  }

  async updateMovies(
    location: string = this.defaultLocation,
    date: string = (new Date()).toDateString(),
    count: number = 10,
  ) {
    const movies = await this.fetchMovies(location, date, count);
    console.log(movies);
    this._movies$.next(movies);
  }

  async updateCinemas(
    location: string = this.defaultLocation,
    count: number = 10,
  ) {
    const cinemas = await this.fetchCinemasNearby(location, count);
    console.log(cinemas);
    this._cinemas$.next(cinemas);
  }
  async updateCinemaShowTimes(
    cinemaId: string = '8923',
    count: number = 10,
  ) {
    const cinemas = await this.fetchCinemaShowTimes(cinemaId, count);
    console.log(cinemas);
    // this._cinemas$.next(cinemas);
  }
}
