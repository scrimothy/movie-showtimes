import { MovieListContainerComponent } from './movie-list/movie-list-container/movie-list-container.component';
import { MovieDetailContainerComponent } from './movie-detail/movie-detail-container/movie-detail-container.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


const routes: Routes = [
  { path: '', component: MovieListContainerComponent },
  { path: 'movie/:id', component: MovieDetailContainerComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
